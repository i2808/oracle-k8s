# Oracle K8S

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/i2808/oracle-k8s)

Set up a free infrastructure on Oracle cloud

## 0: Update k8s

In cluster.tf change kubernetes_version at both places.
Start pipeline (merge into main branch)

In Oracle Cloud console, go in node pool and delete node one at a time -> It recreates with the new version.

Add the tags
"wordpress: apache" on first node
"wordpress: mysql" on other node



## 1: Argo CD

From:
https://github.com/argoproj/argo-cd/releases

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.4.0-rc5/manifests/install.yaml
```

Add _--insecure_ for ingressroute in argocd-server deployment:

```
containers:
- command:
  - argocd-server
  - --insecure
```

Ingressroute:

```
apiVersion: traefik.containo.us/v1alpha1
  kind: IngressRoute
  metadata:
    name: argocd
  spec:
    entryPoints:
    - websecure
    routes:
    - kind: Rule
      match: Host(`argocd.duckdns.org`)
      services:
      - name: argocd-server
        port: 80
    tls:
      secretName: argocd.duckdns.org
```

## 2: Traefik

Manifest ArgoCD:

```
project: default
source:
  repoURL: 'https://helm.traefik.io/traefik'
  targetRevision: 10.20.1
  helm:
    parameters:
      - name: logs.access.enabled
        value: 'true'
      - name: logs.general.level
        value: INFO
    values: |-

      service:
          annotations: {oci.oraclecloud.com/load-balancer-type: nlb}

      ports:
          web:
            redirectTo: websecure

      additionalArguments:
          - "--providers.kubernetescrd"
          - "--providers.kubernetesingress"
          - "--entrypoints.web.http.redirections.entryPoint.to=:443"
          - "--entrypoints.web.http.redirections.entrypoint.permanent=true"
          - "--entrypoints.web.http.redirections.entrypoint.priority=10"
  chart: traefik
destination:
  server: 'https://kubernetes.default.svc'
  namespace: traefik
syncPolicy:
  automated:
    prune: true
    selfHeal: true
  syncOptions:
    - CreateNamespace=true
```

## 3: Cert-manager

Manifest ArgoCD:

```
project: default
source:
  repoURL: 'https://charts.jetstack.io'
  targetRevision: v1.8.0
  helm:
    parameters:
      - name: installCRDs
        value: 'true'
  chart: cert-manager
destination:
  server: 'https://kubernetes.default.svc'
  namespace: cert-manager
syncPolicy:
  automated:
    prune: true
    selfHeal: true
  syncOptions:
    - CreateNamespace=true
```

Create a cluster issuer

```
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
  namespace: cert-manager
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: doublejo@pm.me
    privateKeySecretRef:
      name: letsencrypt
    solvers:
    - http01:
        ingress:
          class: traefik
```
