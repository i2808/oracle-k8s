terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = ">=4.71.0"
    }
  }
}

provider "oci" {
  tenancy_ocid = var.Tenancy_OCID
  user_ocid    = var.User_OCID
  region       = local.region
  private_key  = var.PRIVATE_KEY
  fingerprint  = var.Fingerprint
}
