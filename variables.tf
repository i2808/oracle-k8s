variable "Fingerprint" {
  type = string
}

variable "Tenancy_OCID" {
  type = string
}

variable "User_OCID" {
  type = string
}

variable "PRIVATE_KEY" {
  type = string
}

variable "PUBLIC_KEY_WSL" {
  type = string
}
